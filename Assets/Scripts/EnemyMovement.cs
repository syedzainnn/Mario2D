﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public int enemySpeed;
    private int xMoveDirection = 1;
    Rigidbody2D rb2d;
    SpriteRenderer spriteRend;
    bool facingRight =true;

    private void Start()
    {

        spriteRend = GetComponent<SpriteRenderer>();
        rb2d = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        //RaycastHit2D hit = Physics2D.Raycast(transform.position, new Vector2(xMoveDirection, 0f));
      //  RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.right, 0.01f);
        //Debug.DrawRay(transform.position, Vector2.right, Color.magenta, 0.01f);

           rb2d.velocity = new Vector2(xMoveDirection, 0) * enemySpeed;
          // if (hit.distance < 0.01f)
         // {
           // Debug.Log(hit.distance);
            //Flip();
         //}


       // Vector3 v = startPos;
       // v.x += delta * Mathf.Sin(Time.fixedTime * speed);
       // transform.position = v;
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        switch (collision.gameObject.name)
        {
            case "Wall_1":
                {
                    xMoveDirection = 1;
                    if (facingRight == false)
                    {
                        FlipPlayer();
                        //  Debug.Log(facingRight);
                    }
                }
                break;
            case "Wall_2":
                {
                    xMoveDirection = -1;
                    if (facingRight == true)
                    {
                        FlipPlayer();
                        //Debug.Log(facingRight);
                    }
                }
                break;
            case "Enemy":
                {
                    xMoveDirection = xMoveDirection * -1;
                    FlipPlayer();
                }
                break;
        }
    }


    private void FlipPlayer()
    {
        facingRight = !facingRight;
        Vector2 localScale = gameObject.transform.localScale;
        localScale.x *= -1;
        transform.localScale = localScale;
    }



}




