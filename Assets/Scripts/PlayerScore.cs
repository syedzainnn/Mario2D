﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerScore : MonoBehaviour {
    private float timeLeft = 120;
    public int playerScore = 0;
    public Text Time_Left;
    public Text playerScoreUI;
    public GameObject RestartBUtton;

	// Use this for initialization
	void Start () {
        Time.timeScale = 1;
        Time_Left.text = "Time Left: " + timeLeft;
        playerScoreUI.text = "Score: " + playerScore;
        RestartBUtton.SetActive (false);
        DataManager.dataManager.LoadData();
	}
	
	// Update is called once per frame
	void Update () {
        timeLeft -= Time.deltaTime;
        Time_Left.text = "Time Left: " + (int)timeLeft;
        playerScoreUI.text = "Score: " + playerScore;
        if (timeLeft < 0.1f)
        {
            Restart();
        }
      

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "EndLevel")
        {
            CountScore();
        }
        else if (collision.gameObject.tag == "Coins")
        {
            AudioSource Coin_Audio = GetComponent<AudioSource>();
            Coin_Audio.Play();
            Coin_Audio.Play(44100);
            playerScore = playerScore + 10;
            Destroy(collision.gameObject);
            // collision.gameObject.SetActive(false);
        }
        else if (collision.gameObject.name == "Collider")
        {
         
            playerScore = playerScore + 20;
            collision.transform.parent.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            collision.transform.parent.gameObject.GetComponent<EnemyMovement>().enabled = false;
            collision.transform.parent.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(200, -200))  ;
            //Destroy(collision.transform.parent.gameObject);
            Debug.Log("Squash");
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * 100);
   
        }

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Enemy")
        {

            Debug.Log("Died");
            Destroy(gameObject);
            CountScore();
            DataManager.dataManager.SaveData();

        }
    }

    void CountScore()
    {
        Debug.Log("Data says high score is currently " + " "+ DataManager.dataManager.highScore);
        playerScore = playerScore + (int)(timeLeft *10);
        DataManager.dataManager.highScore = playerScore + (int)(timeLeft * 10);
        DataManager.dataManager.SaveData();
        Debug.Log("After score added to datamanager, Updated score is " + " " +DataManager.dataManager.highScore);
        
        RestartBUtton.SetActive(true);
        Time.timeScale = 0 ;

    }
    public void Restart()
    {
        SceneManager.LoadScene("Prototype_1");
    }
}
