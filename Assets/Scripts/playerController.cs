﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : PhysicsObject {
    public float JumpTakeOffSpeed = 7;
    public float maxSpeed = 7;
    private SpriteRenderer spriterenderer;
    private Transform SpriteTransform;
    private Vector2 localScale;
    private Animator animator;
    private float groundLevel;

   bool squash = false;

	void Awake () {
        spriterenderer = this.GetComponent<SpriteRenderer>();
        // spriterenderer = GetComponentInChildren <SpriteRenderer>();

        //SpriteTransform =this.transform.GetChild(0). GetComponent<Transform>();
        SpriteTransform =this.GetComponent<Transform>();

        // Debug.Log(SpriteTransform.name);
        animator = GetComponent<Animator>();
        groundLevel = transform.position.y;
    }


    protected override void ComputeVelocity()
    {
    
        Vector2 move = Vector2.zero;
        move.x = Input.GetAxis("Horizontal");
        if (move.x != 0)
        {
            animator.SetBool("IsRunning", true);
        }
        else if(move.x ==0)
        {
            animator.SetBool("IsRunning", false);
        }
        

        if (Input.GetButton("Jump"))
        {
            if (grounded == false)
            {
                // localScale = SpriteTransform.localScale;
                // localScale.y = 1.96f;
                // SpriteTransform.localScale = localScale;



                // localScale =  SpriteTransform.localScale;
               //  localScale.y = 0.5f;
               //  SpriteTransform.localScale = localScale;

            }
        }



        if (Input.GetButtonDown("Jump"))
        {
            if (grounded == true)
            {

               // localScale = SpriteTransform.localScale;
               // localScale.y = 1.5f;
               // SpriteTransform.localScale = localScale;
                velocity.y = JumpTakeOffSpeed;
                animator.SetBool("IsJumping",true);
               
                
                
                //  Vector2 localScale = this.transform.localScale;
                //localScale.y = 0.4f;
                // this.transform.localScale = localScale;
            }
        }
        else if (Input.GetButtonUp("Jump"))
        {

            if (velocity.y > 0) {
                velocity.y = velocity.y * 0.5f;
            }
           


        }

        bool flipSprite = (spriterenderer.flipX ? (move.x> 0.01f) : ( move.x< 0.01f));
        if (flipSprite)
       {
            spriterenderer.flipX = !spriterenderer.flipX;
        }
        // animator.SetBool("grounded",grounded);
        //animator.SetFloat("velocityX", Mathf.Abs (velocity.x) /maxSpeed );

        if (grounded)
        {
         //   Vector2 localScale = this.transform.localScale;
           // localScale.y = 0.2f;
          //  this.transform.localScale = localScale;
        }
        targetVelocity = move * maxSpeed;
        }




    IEnumerator Squash ()
    {
        yield return new WaitForSeconds(0.1f);

        localScale = SpriteTransform. localScale;
        localScale.y = 0.2f;
        SpriteTransform.localScale = localScale;


        //localScale = SpriteTransform.localScale;
        //localScale.y = 0.98f;
        //SpriteTransform.localScale = localScale;
        Debug.Log("I AM Called");
    }
    private void LateUpdate()
    {
        if (velocity.y ==0)
        {
            animator.SetBool("IsJumping",false);
        }



        if (grounded == false)
        {
            squash = true;   
        }

        if (squash == true && grounded == true)
        {
           
            localScale =  SpriteTransform.localScale;
            localScale.y = 1f;
            SpriteTransform.localScale = localScale;

           
            
            
            //localScale = SpriteTransform.localScale;
           // localScale.y = 0.49f;
            //SpriteTransform.localScale = localScale;
           // StartCoroutine("Squash");
            squash = false;
        
        }

    }

}



