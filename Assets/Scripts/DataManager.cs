﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class DataManager : MonoBehaviour
{
    public static DataManager dataManager;
    public int highScore;

    private void Awake()
    {
        if (dataManager == null)
        {
            DontDestroyOnLoad(gameObject);
            dataManager = this;
        }
        else if (dataManager != this)
        {
            Destroy(gameObject);
        } 
             
    }

    public void SaveData()
    {
        //Data is Saved
        BinaryFormatter binForm = new BinaryFormatter(); // Creates Binary Formatter
        FileStream file = File.Create(Application.persistentDataPath + "/gameInfo.dat"); // creates File
        gameData data = new gameData();   // Creates container for data
        data.highscore = highScore;
        binForm.Serialize(file,data);
        file.Close();
    }
    public void LoadData()
    {
        if (File.Exists(Application.persistentDataPath + "/gameInfo.dat"))
        {
            BinaryFormatter BinForm = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/gameInfo.dat", FileMode.Open);
            gameData data = (gameData)BinForm.Deserialize(file);
            file.Close();
            highScore = data.highscore;
        }
        //Data is loaded
    }
}


[Serializable]
class gameData
{

    public int highscore;
}
