﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour {
    public bool hasDied;
    public int health;
	// Use this for initialization
	void Start () {
        hasDied = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (gameObject.transform.position.y < -1)
        {
            StartCoroutine("Die");
        }
           
	}
    IEnumerator Die ()
    {
        hasDied = false;
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene("Prototype_1");


    }
}
