﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloud9 : MonoBehaviour {
    public float speed;
    private float xDir;
    Rigidbody2D rb2d;
	// Use this for initialization
	void Start () {
        xDir = transform.position.x;
        rb2d = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        xDir -= Time.deltaTime * speed;
        // transform.position = new Vector3(xDir, transform.position.y, transform.position.z);
        rb2d.velocity= new Vector2(speed,0);

        if (transform.position.x < -15)
        {
            transform.position = new Vector3(29, transform.position.y, transform.position.z);
            if (transform.position.x == 29)
            {
              //  transform.position = new Vector3(xDir, transform.position.y, transform.position.z);

            }

        }

        else if (transform.position.x > 30)
            transform.position = new Vector3(-14f, transform.position.y, transform.position.z);
	}
}
